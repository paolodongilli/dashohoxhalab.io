#+TITLE:     Tips
#+AUTHOR:    Dashamir Hoxha
#+EMAIL:     dashohoxha@gmail.com
#+LANGUAGE:  en
#+OPTIONS:   H:3 num:nil toc:t \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+OPTIONS:   TeX:nil LaTeX:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:info toc:t ltoc:t mouse:#aadddd buttons:0 path:org-info.js
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="org-info.css" />


* Fix screen brightness on wakeup

  When LinuxMint (19.1) wakes up it has a maximum brightness and I
  have to lower it manually each time, which is inconvenient. To fix
  it automatically I have done these:

  - Created a script =/usr/local/bin/fix-backlight.sh= with a content
    like this:
    #+begin_example
    #!/bin/bash
    echo 368 > /sys/class/backlight/intel_backlight/brightness
    echo 51 > '/sys/class/leds/smc::kbd_backlight/brightness'
    #+end_example

  - Made it executable:
    #+begin_example
    chmod +x /usr/local/bin/fix-backlight.sh
    #+end_example
  
  - Created the service =/etc/systemd/system/fix-backlight.service=
    with a content like this:
    #+begin_example
    [Unit]
    Description=Run /usr/local/bin/fix-backlight.sh
    After=suspend.target hibernate.target hybrid-sleep.target

    [Service]
    ExecStart=/usr/local/bin/fix-backlight.sh

    [Install]
    WantedBy=suspend.target hibernate.target hybrid-sleep.target
    #+end_example

  - Enabled this service:
    #+begin_example
    systemctl enable fix-backlight.service
    #+end_example


  Reference: https://unix.stackexchange.com/questions/152039/how-to-run-a-user-script-after-systemd-wakeup

* How to build a VPN

  - https://blog.ssdnodes.com/blog/outline-vpn-tutorial-vps/

* Block adds

  - https://proprivacy.com/guides/use-your-hosts-file-to-block-ads-and-malware
  - https://github.com/StevenBlack/hosts

  #+begin_example
  sudo wget https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts -O /etc/hosts
  #+end_example

* Use Secure DNS

  Edit ~/etc/systemd/resolved.conf~ and place this content:
  #+begin_example
  [Resolve]
  # Use Quad9.net DNS, and Cloudflare DNS.
  # Both supports DNS over TLS and DNSSEC,
  # and promises not to log DNS queries.
  DNS=9.9.9.9 1.1.1.1 2620:fe::fe 2606:4700:4700::1111
  FallbackDNS=149.112.112.112 1.0.0.1 8.8.8.8 8.8.4.4 \
              2620:fe::9 2606:4700:4700::1001 2001:4860:4860::8888 2001:4860:4860::8844
  # Attempt to use DNS over TLS
  DNSOverTLS=opportunistic
  # Enforce DNSSEC validation.
  DNSSEC=true
  #+end_example

  Then restart =systemd-resolved.service=:
  #+begin_example
  systemctl restart systemd-resolved.service
  #+end_example

  Reference:
  - https://www.ctrl.blog/entry/systemd-resolved.html

* Download a single package at a time

  The command `apt upgrade` opens several download connections in
  parallel and sometimes this may be a problem because it saturates
  the network connections and bloks your work. It would be better
  if its download is less aggressive.

  This can be improved with this setting:
  #+begin_example
  echo 'Acquire::Queue-Mode "access";' >/etc/apt/apt.conf.d/75download
  #+end_example

  References:
  - https://www.cyberciti.biz/faq/how-to-speed-up-apt-get-apt-command-ubuntu-linux/
  - https://askubuntu.com/questions/88731/can-the-update-manager-download-only-a-single-package-at-a-time

* Create a Wi-Fi hotspot in less than 10 minutes with Pi Raspberry!

- https://howtoraspberrypi.com/create-a-wi-fi-hotspot-in-less-than-10-minutes-with-pi-raspberry/

If you have installed `ufw` on the Raspberry Pi, make sure to edit
`/etc/default/ufw` and to set `DEFAULT_FORWARD_POLICY="ACCEPT"`.

* Build an OpenVPN container

- https://github.com/kylemanna/docker-openvpn/blob/master/docs/docker-compose.md
- https://www.kennethballard.com/?p=4691

If you want to run this OpenVPN container on a RaspberryPi, you have
to build the image yourself, like this:
#+begin_example
git clone https://github.com/kylemanna/docker-openvpn
cd docker-openvpn
docker build -t kylemanna/openvpn .
#+end_example

* Install font Monaco

#+begin_example
mkdir -p /usr/share/fonts/truetype/Monaco
cd /usr/share/fonts/truetype/Monaco
wget https://github.com/mikesmullin/gvim/raw/master/.fonts/Monaco_Linux.ttf
fc-cache -f .
#+end_example

Reference:
- https://gitlab.com/docker-scripts/guacamole/-/blob/master/Dockerfile#L62

* Set color prompt

On =~/.bashrc= uncomment *force_color_prompt=yes* and set *PS1* like
this:

#+begin_example
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u\[\033[01;33m\]@\[\033[01;36m\]\h \[\033[01;33m\]\w \[\033[01;35m\]\$ \[\033[00m\]'
#+end_example

* Enable bash completion

- Make sure that the package =bash-completion= is installed:
  #+begin_example
  apt install bash-completion
  #+end_example

- Make sure that these lines at the end of =~/.bashrc= are
  uncommented:
  #+begin_example
  # enable programmable completion features (you don't need to enable
  # this, if it's already enabled in /etc/bash.bashrc and /etc/profile
  # sources /etc/bash.bashrc).
  if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
      . /etc/bash_completion
  fi
  #+end_example

* Enable/disable wifi from terminal

#+begin_example
nmcli r wifi on
nmcli r wifi off
#+end_example
