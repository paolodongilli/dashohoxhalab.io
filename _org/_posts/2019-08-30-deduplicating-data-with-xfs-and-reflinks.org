#+TITLE:     Deduplicating Data With XFS And Reflinks
#+AUTHOR:    Dashamir Hoxha
#+EMAIL:     dashohoxha@gmail.com
#+DATE:  2019-08-30
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+OPTIONS:   TeX:nil LaTeX:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc
# #+INFOJS_OPT: view:overview toc:t ltoc:t mouse:#aadddd buttons:0 path:js/org-info.js
#+STYLE: <link rel="stylesheet" type="text/css" href="css/org-info.css" />
#+begin_comment yaml-front-matter
---
layout:  post
title:   Deduplicating Data With XFS And Reflinks
date:    2019-08-30

summary: Copy-on-Write filesystems have the nice property that it is
    possible to "clone" files instantly, by having the new file refer
    to the old blocks, and copying (possibly) changed blocks. This
    both saves time and space, and can be very beneficial in a lot of
    situations (for example when working with big files). In Linux
    this type of copy is called "reflink". We will see how to use it
    on the XFS filesystem.

tags:    filesystem Copy-on-Write XFS reflinks deduplicate
---
#+end_comment

Copy-on-Write filesystems have the nice property that it is possible
to "clone" files instantly, by having the new file refer to the old
blocks, and copying (possibly) changed blocks. This both saves time
and space, and can be very beneficial in a lot of situations (for
example when working with big files). In Linux this type of copy is
called "reflink". We will see how to use it on the XFS filesystem.

* Create a virtual block device

Linux supports a special block device called the loop device, which
maps a normal file onto a virtual block device. This allows for the
file to be used as a "virtual file system". Let's create such a file:
#+begin_example
$ fallocate -l 1G disk.img
$ du -hs disk.img
1001M	disk.img
#+end_example

Now let's create a loop device with this file:
#+begin_example
$ sudo losetup -f disk.img
$ losetup -a
/dev/loop0: []: (/home/user/disk.img)
#+end_example

The option *-f* finds an unused loop device, and =losetup -a= shows
the name of the loop device that was created.

* Create an XFS filesystem with the 'reflink' flag

Let's create an XFS filesystem on it, with the flag *reflink=1* and
the label *test*:
#+begin_example
$ mkfs.xfs -m reflink=1 -L test disk.img
meta-data=disk.img               isize=512    agcount=4, agsize=64000 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=0, rmapbt=0, reflink=1
data     =                       bsize=4096   blocks=256000, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal log           bsize=4096   blocks=1850, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
#+end_example

Now we can mount it on a directory:
#+begin_example
$ mkdir mnt
$ sudo mount /dev/loop0 mnt
$ df -h mnt/
Filesystem      Size  Used Avail Use% Mounted on
/dev/loop0      993M   40M  954M   4% /home/user/mnt
#+end_example

* Copy files with '--reflink'

Let's create for testing a file of size 100 MB (with random data):
#+begin_example
$ sudo chown mnt: user
$ cd mnt/
$ dd if=/dev/urandom of=test bs=1M count=100
100+0 records in
100+0 records out
104857600 bytes (105 MB, 100 MiB) copied, 0,445498 s, 235 MB/s
#+end_example

The command =df -h .= shows us *140M* used:
#+begin_example
$ df -h .
Filesystem      Size  Used Avail Use% Mounted on
/dev/loop0      993M  140M  854M  15% /home/user/mnt
#+end_example

Let’s copy the file with reflinks enabled:
#+begin_example
$ cp -v --reflink=always test test1
'test' -> 'test1'

$ ls -hsl
total 200M
100M -rw-rw-r-- 1 user user 100M Aug 30 10:44 test
100M -rw-rw-r-- 1 user user 100M Aug 30 10:49 test1

$ df -h .
Filesystem      Size  Used Avail Use% Mounted on
/dev/loop0      993M  140M  854M  15% /home/user/mnt
#+end_example

So, each copy of the file is *100M*, but both of them still take on
disk the same amout of space as before (*140M*). This shows the
space-saving feature of reflinks. If the file was big enough, we would
have noticed as well that the reflink copy takes no time at all, it is
done instantly.

* Deduplicate existing files

If there were already normal (non-reflink) copies of the file, we
could deduplicate them with a tool like =duperemove=:
#+begin_example
$ cp test test2
$ cp test test3

$ ls -hsl
total 400M
100M -rw-rw-r-- 1 user user 100M Aug 30 10:44 test
100M -rw-rw-r-- 1 user user 100M Aug 30 10:49 test1
100M -rw-rw-r-- 1 user user 100M Aug 30 11:03 test2
100M -rw-rw-r-- 1 user user 100M Aug 30 11:03 test3

$ df -h .
Filesystem      Size  Used Avail Use% Mounted on
/dev/loop0      993M  340M  654M  35% /home/user/mnt
#+end_example

Now let's install and run =duperemove=
#+begin_example
$ sudo apt install duperemove

$ duperemove -hdr --hashfile=/tmp/test.hash .

$ df -h .
Filesystem      Size  Used Avail Use% Mounted on
/dev/loop0      993M  198M  796M  20% /home/user/mnt
#+end_example

So, it has reduced the amount of disk space used from *340M* to
*198M*.


* Clean up

Unmount and delete the test directory =mnt/=:
#+begin_example
$ cd ..
$ umount mnt/
$ rmdir mnt/
#+end_example

Delete the loop device:
#+begin_example
$ losetup -a
$ sudo losetup -d /dev/loop0
#+end_example

Remove the file that was used to create the loop device:
#+begin_example
$ rm disk.img
#+end_example

* References

- https://www.thegeekdiary.com/how-to-create-virtual-block-device-loop-device-filesystem-in-linux/
- https://hooks.technology/2018/03/xfs-deduplication-with-reflinks/
