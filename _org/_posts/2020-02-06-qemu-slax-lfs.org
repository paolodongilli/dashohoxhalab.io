#+TITLE:     Trying LFS with Slax and QEMU
#+AUTHOR:    Dashamir Hoxha
#+EMAIL:     dashohoxha@gmail.com
#+DATE:  2020-02-06
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+OPTIONS:   TeX:nil LaTeX:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc
# #+INFOJS_OPT: view:overview toc:t ltoc:t mouse:#aadddd buttons:0 path:js/org-info.js
#+STYLE: <link rel="stylesheet" type="text/css" href="css/org-info.css" />
#+begin_comment yaml-front-matter
---
layout:  post
title:   Trying LFS with Slax and QEMU
date:    2020-02-06

summary: Slax is a modern, portable, small and fast Linux operating
    system with modular approach and outstanding design. It can also
    run nicely on a QEMU machine. This environment can be a nice place
    to try the instructions of Linux From Scratch. In this article we
    will see how.

tags: LFS QEMU Slax
---
#+end_comment


[[https://www.slax.org/][Slax]] is a modern, portable, small and fast Linux operating system with
modular approach and outstanding design. It can also run nicely on a
[[https://www.qemu.org/][QEMU]] machine. This environment can be a nice place to try the
instructions of [[http://www.linuxfromscratch.org/lfs/][Linux From Scratch]]. In this article we will see how.

* Install Slax on a QEMU virtual machine

First of all we need to install QEMU. It is as simple as:
#+begin_example
sudo apt install qemu
#+end_example

Next download the latest stable version of Slax from
https://www.slax.org/.  At the time of writing it is
[[http://ftp.sh.cvut.cz/slax/Slax-9.x/slax-32bit-9.11.0.iso][slax-32bit-9.11.0.iso]] (based on Debian 9). (You may also try to get
the testing version, based on Debian 10, from here:
https://www.slax.org/blog.php)

Give it a try like this:
#+begin_example
sudo qemu-system-i386 \
         -enable-kvm \
         -m 512 \
         -boot d \
         -cdrom slax-32bit-9.11.0.iso
#+end_example

The option =-enable-kvm= makes it run faster, but it also requires
=sudo=. Slax runs well even with less than 512MB memory. We are
telling the virtual machine to boot from the CD drive, and to use the
Slax iso as a live CD.

We will also need a virtual disk for installing Slax. Let's create
one, and then create a partition table on it:
#+begin_example
qemu-img create slax.img 2G
echo "o,n,p,1,,,w" | tr , "\n" | fdisk slax.img
fdisk -l slax.img
#+end_example

Now let's start the virtual machine again, with the virtual disk
attached:
#+begin_example
sudo qemu-system-i386 \
         -enable-kvm \
         -m 512 \
         -boot d \
         -cdrom slax-32bit-9.11.0.iso \
         -drive file=slax.img,format=raw
#+end_example

On the green desktop of Slax we can right-click and open a terminal
(or click at the start icon on the bottom). With the command =lsblk=
we can confirm that we have the disk ~sda~ of size =2G= and the
partition ~sda1~. We need to format this partition:
#+begin_example
lsblk
lsblk /dev/sda
mkfs.ext4 -L Slax /dev/sda1
#+end_example

To install Slax on ~/dev/sda1~ we just need to copy the directory
~/slax/~ from ~/media/sr0/~ (the mounted CD), and then make the disk
~sda~ bootable by running the script ~bootinst.sh~ in the
~/slax/boot/~ directory:
#+begin_example
df -h
ls /media/sr0/
ls /media/sr0/slax/
cp -a /media/sr0/slax/ /media/sda1/
cd /media/sda1/slax/
ls
cd boot/
ls
./bootinst.sh
#+end_example

Slax is now installed on ~slax.img~, so we can shut down the virtual
machine and start another one without the cdrom and the iso file:
#+begin_example
sudo qemu-system-i386 \
         -enable-kvm \
         -m 1024 \
         -drive file=slax.img,format=raw
#+end_example

We can now update the system and install new packages (we couldn't do
persistent changes on the live system started from the iso file):
#+begin_example
df -h
du -hs .
free -h

apt update
apt upgrade
apt install vim

systemctl enable ssh
systemctl start ssh
systemctl status ssh
#+end_example

The last commands are for starting the =openssh-server=. However, in
order to be able to ssh to the virtual machine, we also need to
forward a port to 22 when starting it, like this:
#+begin_example
sudo qemu-system-i386 -enable-kvm -m 1024 \
         -drive file=slax.img,format=raw \
         -redir tcp:10022::22
#+end_example

Now we can ssh into the virtual machine like this:
#+begin_example
ssh -p 10022 root@localhost
#+end_example

The default password (if you haven't changed it yet) is =toor= (=root=
backwards). Working from a SSH terminal is more convenient for
copy-pasting commands.

* Preparing for LFS

We need a partition of about 10G for building LFS. Let's create a new
virtual disk, and a partition inside it:
#+begin_example
qemu-img create lfs.img 10G
echo "o,n,p,1,,,w" | tr , "\n" | fdisk lfs.img
fdisk -l lfs.img
#+end_example

Now we can start the virtual machine again, connecting to it this
second drive as well:
#+begin_example
sudo qemu-system-i386 -enable-kvm -m 1024 \
         -redir tcp:10022::22 \
         -drive file=slax.img,format=raw \
         -drive file=lfs.img,format=raw
#+end_example

After logging in (with =ssh -p 10022 root@localhost=), we can verify
that the second drive appears inside it as =sdb=, and we can format
=/dev/sdb1=:
#+begin_example
lsblk
mkfs.ext4 -L LFS /dev/sdb1
df -h
df -h /media/sdb1/
#+end_example

Now we need to follow the instructions on
http://www.linuxfromscratch.org/lfs/view/stable/chapter02/hostreqs.html
to check for the software required on the host system. It is just a
copy-paste. The script =version-check.sh= tells us that some needed
tools are missing. Let's install them and check again:
#+begin_example
apt install \
    binutils bison gawk gcc g++ m4 make patch python3 texinfo
bash version-check.sh
#+end_example

Next, we need to use =/media/sdb1= as the LFS directory, as described
on
http://www.linuxfromscratch.org/lfs/view/stable/chapter02/aboutlfs.html
#+begin_example
export LFS=/media/sdb1
#+end_example

Then follow the rest of the steps and instructions as presented on the
book. In the end, when you have to reboot to try the new system, you
can actually boot a system that contains only the LFS disk, like this:
#+begin_example
sudo qemu-system-i386 -enable-kvm -m 1024 \
         -drive file=lfs.img,format=raw
#+end_example

However the ~lfs.img~ disk will appear on the new system as ~/dev/sda~
(not as ~/dev/sdb~), so you have to be careful when you set up the
GRUB configuration file
(http://www.linuxfromscratch.org/lfs/view/stable/chapter08/grub.html)
For example use =set root=(hd0,1)= and =root=/dev/sda1=.
