#+TITLE:     Central Git Repo With SSH
#+AUTHOR:    Dashamir Hoxha
#+EMAIL:     dashohoxha@gmail.com
#+DATE:  2020-08-13
#+OPTIONS:   H:3 num:nil toc:nil \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+OPTIONS:   TeX:nil LaTeX:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc
# #+INFOJS_OPT: view:overview toc:t ltoc:t mouse:#aadddd buttons:0 path:js/org-info.js
#+STYLE: <link rel="stylesheet" type="text/css" href="css/org-info.css" />
#+begin_comment yaml-front-matter
---
layout:  post
title:   Central Git Repo With SSH
date:    2020-08-13

summary: This tutorial shows how to create a bare Git repository on
    your own server and how to access it through ssh.

tags:    Git SSH
---
#+end_comment

*Note:* This tutorial can be tried interactively on:
https://katacoda.com/dashohoxha/courses/misc/central-git-repo-with-ssh

* Introduction

To synchronize the work on a programming project we usually use
GitHub, GitLab, etc. It is also possible to install and use one of the
GitHub-like systems in your own server, like:
- https://gitea.io/
- https://about.gitlab.com/install/
- https://gitbucket.github.io/
- https://kallithea-scm.org/
- http://gitprep.yukikimoto.com/
- https://git.wiki.kernel.org/index.php/Gitweb
- http://gitblit.github.io/gitblit/
- https://gitlist.org/

However if you don't need a web interface, issue management, pull
requests, and other fancy features, you can easily create a central
Git repository on your own server and access it through ssh.  This
tutorial shows how to do it.

* Step 1 - Setup the central server

#+CAPTION:    Central Git Repo With SSH
#+NAME:       fig:ssh-git
[[file:images/central-git-repo-with-ssh/ssh-git.png][file:images/central-git-repo-with-ssh/ssh-git.png]]

In this step we are going to setup the server. In the following steps
we will do the setup for the first user and the second user.

1. Create user accounts. For each user that needs to access the
   central repo we will create an account on the server. Let's assume
   that we have two users and their accounts are named =user1= and
   =user2=.

   #+begin_example
   useradd -m -s /bin/bash user1
   echo user1:pass1 | chpasswd
   useradd -m -s /bin/bash user2
   echo user2:pass2 | chpasswd
   #+end_example
   
2. Create a group for Git and add the users to this group:

   #+begin_example
   addgroup git-group
   adduser user1 git-group
   adduser user2 git-group
   #+end_example

3. Create a bare Git repository for the project:

   #+begin_example
   git init --bare --shared /srv/project.git
   #+end_example
   
   Central Git repositories don't have a working tree. They are called
   "bare" repositories and are initialized with the option =--bare=.
   
   The option =--shared= specifies that this Git repository is to be
   shared amongst several users. This allows users belonging to the
   same group to push into this repository.
   
4. Set the right group ownership to this repo:

   #+begin_example
   chgrp -R git-group /srv/project.git
   chmod -R g+rw /srv/project.git
   chmod -R g+s /srv/project.git
   ls -al /srv/project.git
   #+end_example
   
   Notice the =setgid= permission that we set on the repo dir. Because
   of it, the new sub-directories or files that are created inside the
   repo dir will have the same group as the repo dir
   (=git-group=). This is convenient for a group of users that use the
   same directory.

* Step 2 - First user setup

For the sake of example, the first user is named =first-user=. He
is going to use the account =user1= on the server.

The following steps should be done on the computer of the first user.

1. Set up the ssh config for the central Git server:
   
   #+begin_example
   mkdir ~/.ssh
   chmod 700 ~/.ssh/
   #+end_example
   
   SSH client configurations are usually kept on the directory
   =~/.ssh/=, and it should be accessible only by the owner.
   
   #+begin_example
   cat <<EOF >> ~/.ssh/config
   Host git-server
       HostName host01
       User user1
       IdentityFile ~/.ssh/git-server
       IdentitiesOnly yes 
   EOF
   #+end_example
   
   In this configuration entry we describe the details for the
   *Host* (server) named =git-server=, which is an arbitrary name
   (we choose it). Its *HostName* in our example is =host01= (which
   in our case resolves to the localhost, however usually it is a FQDN
   or an IP).
   
   The *User* account on the server =host01= is =user1=, and we will
   login there with an SSH key (*IdentityFile*) that is located on
   =~/.ssh/git-server=.
   
2. The SSH key above doesn't exist yet, so let's create it:

   #+begin_example
   ssh-keygen -t rsa -q -N '' -f ~/.ssh/git-server
   #+end_example
   
   The option =-N ''= tells the command to use no passphrase for
   encrypting the private key, and =-f ~/.ssh/git-server= gives it the
   filename where the key should be saved.
   
3. In order to be able to login to the server with this key, we need
   to send the public part of it to the server:
   
   #+begin_example
   ssh-copy-id -i ~/.ssh/git-server.pub git-server
   #+end_example
   
   Now let's try to *ssh* to the server with the new key (should be
   able to do it without a password):
   
   #+begin_example
   ssh git-server ls -al .ssh/
   ssh git-server cat .ssh/authorized_keys
   #+end_example
   
   The command =ls -al .ssh/= is running on the server, through ssh.
   
   Notice that the public key that we sent to the server has been
   appended to ~.ssh/authorized_keys~ on the home directory of
   =user1=.
   
4. Now that we configured an SSH connection to the server, let's
   create a test Git project (on the computer of =first-user=):

   #+begin_example
   mkdir project
   cd project/
   echo '# Test project' > README.md
   git init
   git add .
   git commit -m 'Initial commit'
   #+end_example
   
5. Finally, let's set the remote and push this project to the server:

   #+begin_example
   git remote add origin git-server:/srv/project.git
   git push --set-upstream origin master
   #+end_example

* Step 3 - Second user setup

The setup for the second user is similar to the first one.  The
following steps should be done on the computer of the second user.

1. Set up the ssh config for the central Git server:
   
   #+begin_example
   mkdir ~/.ssh
   chmod 700 ~/.ssh/
   cat <<EOF >> ~/.ssh/config
   Host git-server
       HostName host01
       User user2
       IdentityFile ~/.ssh/git-server
       IdentitiesOnly yes 
   EOF
   #+end_example
   
2. Create an SSH key with the filename (=~/.ssh/git-server=) that we
   used above:

   #+begin_example
   ssh-keygen -t rsa -q -N '' -f ~/.ssh/git-server
   #+end_example
   
3. Send the public key to the server:
   
   #+begin_example
   ssh-copy-id -i ~/.ssh/git-server.pub git-server
   #+end_example
   
   Now try to *ssh* to the server with the new key (should be able to
   do it without a password):
   
   #+begin_example
   ssh git-server ls -al .ssh/
   ssh git-server cat .ssh/authorized_keys
   #+end_example
   
4. Clone the Git project:

   #+begin_example
   git clone git-server:/srv/project.git
   cd project/
   git remote -v
   git log
   #+end_example
   
   This time we didn't have to add a *remote*, because the =origin=
   remote is added automatically when we clone the project.

* Step 4 - Share Code

With this setup we can collaborate and share code by using the central
Git repo as a coordinator.

1. Make some changes to the project, then commit and push it:

   #+begin_example
   cat << EOF >> README.md
   
   First line
   EOF

   git add .
   git commit -m 'Update README'
   git push
   #+end_example
      
2. Now switch to the first user and pull these changes:

   #+begin_example
   cd ~/project/
   git pull
   cat README.md
   git log
   #+end_example
   
